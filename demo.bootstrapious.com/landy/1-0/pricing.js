var pricingInt = {
	init: function() {
		var positionOnSlider = 0;
		var positionOnSliderAnnual = 0;
		$('#monthlySelect').click(function(e) {
			e.preventDefault();
			if (!$(this).hasClass("selected")) {
				positionOnSlider = positionOnSliderAnnual;
				switchToMonthlyPlan(positionOnSlider);
				$("#monthly-slider .pricing-slider-button").css('left', positionOnSlider * 1.0 + "%");
				$('#monthly-slider .progress').css('left', positionOnSlider * 1.0 + "%");
				$(this).addClass('selected');
				$('#annualSelect').removeClass('selected').addClass('opaq');
				$('#annual-slider').hide();
				$('#monthly-slider').show();
			}
		});
		$('#annualSelect').click(function(e) {
			e.preventDefault();
			if (!$(this).hasClass("selected")) {
				positionOnSliderAnnual = positionOnSlider;
				switchToAnnualPlan(positionOnSliderAnnual);
				$("#annual-slider .pricing-slider-button").css('left', positionOnSliderAnnual * 1.0 + "%");
				$('#annual-slider .progress').css('left', positionOnSliderAnnual * 1.00 + "%");
				$(this).removeClass('opaq').addClass('selected');
				$('#monthlySelect').removeClass('selected');
				$('#annual-slider').show();
				$('#monthly-slider').hide();
			}
		});
		$('#monthly-slider .slider-step').click(function(e) {
			e.preventDefault();
			positionOnSlider = $(this).data("position");
			switchToMonthlyPlan(positionOnSlider);
			$("#monthly-slider .pricing-slider-button").css('left', positionOnSlider * 1.0 + "%");
			$('#monthly-slider .progress').css('left', positionOnSlider * 1.0 + "%");
		});
		$('#annual-slider .slider-step').click(function(e) {
			e.preventDefault();
			positionOnSliderAnnual = $(this).data("position");
			switchToAnnualPlan(positionOnSliderAnnual);
			$("#annual-slider .pricing-slider-button").css('left', positionOnSliderAnnual * 1.0 + "%");
			$('#annual-slider .progress').css('left', positionOnSliderAnnual * 1.0 + "%");
		});
		//MONTHLY SLIDER
		$("#monthly-slider .pricing-slider-button").draggable({
			containment: "#monthly-slider .pricing-slider-wrapper",
			scroll: false,
			axis: "x",
			drag: function() {
				$('.pricing-slider-button').removeClass('active-drag');
				positionOnSlider = Math.round(parseInt($("#monthly-slider .pricing-slider-button").position().left) / ($("#monthly-slider .pricing-slider-wrapper").width() / 100))
				$('#monthly-slider .progress').css('left', $("#monthly-slider .pricing-slider-button").position().left + 15 + "px");
				/*if(positionOnSlider >= 100) {
				  $("#monthly-slider .pricing-slider-button").css({left: $("#monthly-slider .pricing-slider-wrapper").width() - 15 + "px"});
				  positionOnSlider = 100;
				  return false;
				}*/
				switchToMonthlyPlan(positionOnSlider);
			},
		});
		//YEARLY SLIDER
		$("#annual-slider .pricing-slider-button").draggable({
			containment: "#annual-slider .pricing-slider-wrapper",
			scroll: false,
			axis: "x",
			drag: function() {
				$('.pricing-slider-button').removeClass('active-drag');
				positionOnSliderAnnual = Math.round(parseInt($("#annual-slider .pricing-slider-button").position().left) / ($("#annual-slider .pricing-slider-wrapper").width() / 100))
				switchToAnnualPlan(positionOnSliderAnnual);
				//move progress bar
				$('#annual-slider .progress').css('left', $("#annual-slider .pricing-slider-button").position().left + 15 + "px");
			}
		});

		function getIndex(positionOnSlider) {
			var index = 0;
			if (positionOnSlider > 85.2) {
				index = 12;
				console.log(positionOnSlider);
				document.getElementById("pro-price").style.fontSize = (positionOnSlider+10) +  'px';
				// $("pro-price").style.fontSize = positionOnSlider;
			} else if (positionOnSlider > 78.1) {
				index = 11;
				document.getElementById("pro-price").style.fontSize = (positionOnSlider+10) +  'px';
			} else if (positionOnSlider > 71) {
				index = 10;
				document.getElementById("pro-price").style.fontSize = (positionOnSlider+10) +  'px';
			} else if (positionOnSlider > 63.9) {
				index = 9;
				document.getElementById("pro-price").style.fontSize = (positionOnSlider+10) +  'px';
			} else if (positionOnSlider > 56.8) {
				index = 8;
				document.getElementById("pro-price").style.fontSize = (positionOnSlider+10) +  'px';
			} else if (positionOnSlider > 49.7) {
				index = 7;
				document.getElementById("pro-price").style.fontSize = (positionOnSlider+10) +  'px';
			} else if (positionOnSlider > 42.6) {
				index = 6;
				document.getElementById("pro-price").style.fontSize = (positionOnSlider+10) +  'px';
			} else if (positionOnSlider > 35.5) {
				index = 5;
				document.getElementById("pro-price").style.fontSize = (positionOnSlider+10) +  'px';
			} else if (positionOnSlider > 28.4) {
				index = 4;
				document.getElementById("pro-price").style.fontSize = (positionOnSlider+20) +  'px';
			} else if (positionOnSlider > 21.3) {
				index = 3;
				document.getElementById("pro-price").style.fontSize = (positionOnSlider+20) +  'px';
			} else if (positionOnSlider > 14.2) {
				index = 2;
				document.getElementById("pro-price").style.fontSize = (positionOnSlider+20) +  'px';
			} else if (positionOnSlider > 7.1) {
				index = 1;
				document.getElementById("pro-price").style.fontSize = (positionOnSlider+20) +  'px';
			} else if (positionOnSlider >= 0) {
				index = 0;
			}
			return index;
		}

		function setPricingVariables(pricing, index) {
			var emailMultiplier = 2.5;
			$('.user-count').text(numberWithCommas(pricing[index].userCount));
			$('.email-count').text(numberWithCommas(pricing[index].userCount * emailMultiplier));
			$('.basic-price').text(pricing[index].basePrice);
			$('.pro-price').text(pricing[index].proPrice);
		}

		function numberWithCommas(number) {
			return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		}

		function pushPricing(positionOnSlider, pricing) {
			if ($(window).width() > 767) {
				$('.pricing-box-wrapper .small').text('/month');
			} else {
				$('.pricing-box-wrapper .small').text('/mo');
			}
			$('.basic-h5 .small').text('/month');
			$('.pro-h5 .small').text('/month');
			if (positionOnSlider <= 7.1) {
				$('.basic-h5 .small').text('');
			}
			var index = getIndex(positionOnSlider);
			setPricingVariables(pricing, index);
			if (index == 0) $('.basic-price').text("FREE");
			// if (positionOnSlider > 92.3) {
			// 	$('.user-count').text(' Unlimited ');
			// 	$('.email-count').text(' Unlimited ');
			// 	$('.basic-price').html(' <a class="more-contact" href="">Contact Us</a> ');
			// 	$('.basic-h5 .small').text('');
			// 	$('.pro-price').html('<a class="more-contact" href="">Contact Us</a>');
			// 	$('.pro-h5 .small').text('');
			// 	/*$('.pricing-box .user-count').parent().html('Unlimited users tracked monthly');
			// 	$('.pricing-box .emails-limit').parent().html('Unlimited emails monthly');*/
			// }
		}
		var pricing
		var switchToMonthlyPlan = function(positionOnSlider) {
			pricing = [
				{"userCount": 500, "basePrice": "FREE", "proPrice": "FREE"},
				{"userCount": 1000, "basePrice": "FREE", "proPrice": "FREE"},
				{"userCount": 5000, "basePrice": "FREE ", "proPrice": " FREE"},
				{"userCount": 10000, "basePrice": " FREE", "proPrice": " FREE "},
				{"userCount": 15000, "basePrice": " FREE", "proPrice": " FREE"},
				{"userCount": 20000, "basePrice": " FREE", "proPrice": "FREE"},
				{"userCount": 25000, "basePrice": " FREE", "proPrice": " FREE"},
				{"userCount": 30000, "basePrice": " FREE", "proPrice": " FREE"},
				{"userCount": 35000, "basePrice": " FREE", "proPrice": " FREE"},
				{"userCount": 40000, "basePrice": " FREE", "proPrice": " FREE"},
				{"userCount": 45000, "basePrice": " FREE", "proPrice": "FREE "},
				{"userCount": 50000, "basePrice": " FREE", "proPrice": " FREE"},
				{"userCount": "50000+", "basePrice": " FREE", "proPrice": "  FREE"},
			];
			pushPricing(positionOnSlider, pricing);
		}
		var switchToAnnualPlan = function(positionOnSlider) {
			pricing = [
				{"userCount": 500, "basePrice": "FREE", "proPrice": "FREE"},
				{"userCount": 1000, "basePrice": "FREE", "proPrice": "FREE"},
				{"userCount": 5000, "basePrice": "FREE ", "proPrice": " FREE"},
				{"userCount": 10000, "basePrice": " FREE", "proPrice": " FREE "},
				{"userCount": 15000, "basePrice": " FREE", "proPrice": " FREE"},
				{"userCount": 20000, "basePrice": " FREE", "proPrice": "FREE"},
				{"userCount": 25000, "basePrice": " FREE", "proPrice": " FREE"},
				{"userCount": 30000, "basePrice": " FREE", "proPrice": " FREE"},
				{"userCount": 35000, "basePrice": " FREE", "proPrice": " FREE"},
				{"userCount": 40000, "basePrice": " FREE", "proPrice": " FREE"},
				{"userCount": 45000, "basePrice": " FREE", "proPrice": "FREE "},
				{"userCount": 50000, "basePrice": " FREE", "proPrice": " FREE"},
				{"userCount": "50000+", "basePrice": " FREE", "proPrice": "  FREE"},
				
			];
			pushPricing(positionOnSlider, pricing);
		}
		switchToMonthlyPlan(28.4);
		$(".pricing-wrapper .pricing-slider-button").css("left", "28.4%");
		$(".pricing-wrapper .progress").css("left", "28.4%");
	}
};
//PRICING SLIDER INIT
if ($('#pricing-wrapper').length > 0) {
	pricingInt.init();
}